package com.example.gs3d;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gs3dApplication {

    public static void main(String[] args) {
        SpringApplication.run(Gs3dApplication.class, args);
    }

}
